/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.danielifg.jax.rsexample;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Daniel
 * Use '?' to specify that an argument for a @QueryParam 'parameter' follows 
 * Use '&' for concatenation of @queryParams 
 * 
 */
@Path("O/{pathParam1}") //  any value as "{pathParam1}" is necessary to access the service class "endpoint".
public class OResource {

    @Context
    private UriInfo context;
    public OResource() { }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(   
                 @PathParam  ("pathParam1")String pathParam,// same "pathParam1" as parameter for the method to use it and print it 
                 @QueryParam("query1")       String queryParam1,  //Use same @QueryParam name to call it in the URI. In this case "query1" and "query2" with "=" for value.
                 @QueryParam("query2")       String queryParam2) {
       
        return "It works! "+"pathParam1="+pathParam+ "  query1="+ queryParam1 + "  query2="+ queryParam2;
    }            
}

// Access URL. http://localhost:8080/JAX-RSexample/webresources/O/Value?query1=ONE&query2=TWO